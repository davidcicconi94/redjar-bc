import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.models.';
import { UserService } from '../user.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
})
export class AddComponent implements OnInit {
  constructor(
    private _formbuilder: FormBuilder,
    private _userservice: UserService
  ) {}

  get id() {
    return this.form.controls['id'];
  }

  get name() {
    return this.form.controls['name'];
  }

  get username() {
    return this.form.controls['username'];
  }

  get phone() {
    return this.form.controls['phone'];
  }

  user: User = {} as User;

  form: FormGroup = this._formbuilder.group({
    id: [''],
    name: ['', Validators.required],
    username: ['', Validators.required],
    phone: ['', Validators.required],
  });

  ngOnInit(): void {}

  newUser() {
    if (this.form.valid) {
      this.user.id = this.id.value;
      this.user.name = this.name.value;
      this.user.username = this.username.value;
      this.user.phone = this.phone.value;
      this._userservice.postUser(this.user).subscribe({
        next: (data) => console.log(data),
        error: (err) => console.log(err),
      });
    } else {
      alert('ERROR, FALTAN CAMPOS POR COMPLETAR');
    }
  }

  refresh() {
    this.form.patchValue({
      id: '',
      name: '',
      username: '',
      phone: '',
    });
  }
}
