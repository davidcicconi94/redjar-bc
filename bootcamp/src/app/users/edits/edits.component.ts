import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-edits',
  templateUrl: './edits.component.html',
  styleUrls: ['./edits.component.css'],
})
export class EditsComponent implements OnInit {
  constructor(
    private _userservice: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  form: FormGroup = new FormGroup({
    id: new FormControl(),
    name: new FormControl(),
    username: new FormControl(),
    phone: new FormControl(),
  });
  id!: number;

  ngOnInit(): void {
    this.id = this._route.snapshot.params['id'];
    console.log(this.id);
    if (Number(this.id)) {
      this._userservice.getUserById(this.id).subscribe({
        next: (us) => {
          this.form.controls['id'].patchValue(us.id);
          this.form.controls['id'].disable();
          this.form.controls['name'].patchValue(us.name);
          this.form.controls['username'].patchValue(us.username);
          this.form.controls['phone'].patchValue(us.phone);
        },
        error: (err) => console.log('Error'),
      });
    } else {
      console.log('Invalid ID');
    }
  }

  editUser() {
    let user = this.form.value;
    console.log(user);

    this._userservice.putUser(this.id, user).subscribe({
      next: () => alert('User edited!'),
      error: () => alert('ERROR'),
      complete: () => this._router.navigate(['/users']),
    });
  }

  goToUsers() {
    this._router.navigate(['/users']);
  }
}
