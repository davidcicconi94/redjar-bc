import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { AddComponent } from './add/add.component';
import { EditsComponent } from './edits/edits.component';
import { DetailsComponent } from './details/details.component';
import { ListComponent } from './list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { UtilsModule } from '../utils/utils.module';

@NgModule({
  declarations: [AddComponent, EditsComponent, DetailsComponent, ListComponent],
  imports: [CommonModule, UsersRoutingModule, HttpClientModule, UtilsModule],
  providers: [UserService],
})
export class UsersModule {}
