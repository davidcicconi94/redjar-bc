import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './models/user.models.';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private _httpclient: HttpClient) {}

  private baseUrl = 'https://jsonplaceholder.typicode.com/users';

  getUsers(): Observable<User[]> {
    return this._httpclient.get<User[]>(this.baseUrl);
  }

  getUserById(id: number): Observable<User> {
    return this._httpclient.get<User>(`${this.baseUrl}/${id}`);
  }

  deleteUserById(id: number): Observable<User> {
    return this._httpclient.delete<User>(`${this.baseUrl}/${id}`);
  }

  postUser(user: User): Observable<User> {
    return this._httpclient.post<User>(`${this.baseUrl}`, user);
  }

  putUser(id: number, user: User) {
    return this._httpclient.put<User>(`${this.baseUrl}/${id}`, user);
  }
}
