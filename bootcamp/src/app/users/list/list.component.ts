import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/user.models.';
import { Router } from '@angular/router';
import { ComunicationService } from 'src/app/services/comunication.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  constructor(
    private _userservice: UserService,
    private _route: Router,
    private _comunicationservice: ComunicationService
  ) {}
  displayedColumns: string[] = ['id', 'name', 'username', 'phone', 'action'];
  // dataSource = ELEMENT_DATA;

  ngOnInit(): void {
    this.getUser();
  }

  @Input() entrada: string = 'USERS LIST';
  userList: User[] = [];

  // Methods
  getUser() {
    this._userservice.getUsers().subscribe({
      next: (data) => {
        this.userList = data;
        console.log(this.userList);
      },
    });
  }

  goToAddUser() {
    this._route.navigate([`users/add`]);
  }

  detailsUser(user: User) {
    this._comunicationservice.emitTitle(user);
  }

  goToEdit(id: number) {
    this._route.navigate([`users/edits/${id}`]);
  }

  deleteUser(id: number) {
    this._userservice.deleteUserById(id).subscribe({
      next: () => {
        this.userList = this.userList.filter((x) => x.id != id);
      },
      error: (err) => console.log(err),
    });
  }
}
