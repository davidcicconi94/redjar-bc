import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { Subject } from 'rxjs';
import { ComunicationService } from 'src/app/services/comunication.service';
import { User } from '../models/user.models.';
import { UserService } from '../user.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit, OnDestroy {
  observable: Subject<User> = new Subject();

  @Input() user: User = {} as User;
  @Output() salida: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _userservice: UserService,
    private _comunicationservice: ComunicationService
  ) {}

  ngOnInit(): void {
    this.observable = this._comunicationservice.$user;

    this.observable.subscribe({
      next: (data) => (this.user = data),
    });
  }

  sendText(text: any) {
    this.salida.emit(text.target.value);
  }

  ngOnDestroy(): void {
    this.observable.unsubscribe();
  }
}
