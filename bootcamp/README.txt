// INSTALACIÓN 

- Angular material: ng add @angular/material

// ESTRUCTURA 
1- Creamos el módulo "users" con routing: ng g m users --routing --skipTests
2- Crearemos los componentes de add, edits, details
3- Crearemos las carpetas "services" (donde irán los servicios) y la carpeta Utils, donde importaremos y exportaremos los módulos de Angular Material.
4- Crearemos la carpeta "models" que tendrá el archivo ts que guardará la interface User, dentro de la carpeta Users.
El servicio para el routeo irá suelto en la carpeta de users, será "user.service.ts"


// ROUTEO
1- Primero nos dirigimos al "app-routing" donde agregaremos a la constante routes de tipo Routes, el path de users y un loadChildren (lazy loading).
2- Cargamos el path y component para el resto, en "users-routing".
3- RECORDAR: el "router-outlet" en el app.html.


// SERVICIO E INYECCION
1- Vamos al archivo "user.service.ts" y lo primero que hacemos es importar el servicio inyectable HttpClient.
2- Luego utilizaremos los métodos REST con la propiedad "this._httpclient.httpMethod<tipoObservable>(url)"


// TABLAS CON ANGULAR MATERIAL 
1- Para verificar que angular material está funcionando, vamos a importar un slider (MatSliderModule) en nuestra app.module.ts y luego pegaremos en el html: 
<mat-slider min="1" max="100" step="1" value="50"></mat-slider>. PEROOOO, vamos a utilizar el modulo "utils" para nuestros componentes Material. Por ende lo importaremos ahi, para luego importar UtilsModule en "users.module.ts"
2- La tabla tendrá como table headers a: id, name, username, phone y actions.
3- En actions irán los botones/iconos de "edit", "delete", "view". 
El boton de editar nos llevará a otro formulario donde en los campos encontraremos los datos del usuario para ser modificados.
4- Al haber 3 eventos click, tendremos que crear 3 metodos de la clase. Y asi, 3 observables en el user.service
5- Agregaremos un boton arriba de la tabla que sea para agregar usuario, eso nos reedireccionara a otro formulario, para crear el nuevo elemento.

6- En los servicios crearemos los métodos con su observable (deberá RETORNARSE), para luego ser itilizado en un nuevo metodo en el archivo ts.


// ADD COMPONENT: formulario
1- Vamos a usar Bootstrap, copiaremos la cdn en index.html
2- En el html crearemos la estructura del formulario, con el div de form-group para cada label e input y la clase form-control para este último.
El form tendrá un evento de tipo ngSubmit que nos llevará al método newUser() del ts.
Ahí, deberemos corroborar si el formulario es válido, entonces el valor de los campos se van a almacenar en la variable user:User, en forma de objeto. Posteriormente nos vamos a subscribir al servicio del metodo postUser(this.user)

En el HTML: 
1- En el form vamos a agregar 
2- En cada input pondremos un formControlName que nos va a conectar con las propiedades que se encuentran en el form del ts 


